import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

// Layout Reference:
// https://www.figma.com/file/1SxgOMojOB2zYT0Mdk28lB/Ecoleta?node-id=136%3A548

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);
