import axios from 'axios'

const api = axios.create({
  // TODO: Use local and prod url
  baseURL: 'http://localhost:3333'
})

export default api