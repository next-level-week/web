import axios from 'axios'

const baseURL = 'https://servicodados.ibge.gov.br/api/v1/'

// TODO: usar esse modo de export no meu repositorio base de cra
export const apiLocations = axios.create({
  baseURL: baseURL + 'localidades/'
})

export const apiCitiesPerUF = axios.create({
  baseURL: baseURL + 'localidades/estados/'
})
