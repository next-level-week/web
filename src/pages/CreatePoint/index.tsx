import React, { useState, useEffect, ChangeEvent, FormEvent } from 'react'
import { Link, useHistory } from 'react-router-dom'
import { FiArrowLeft } from 'react-icons/fi'
import { Map, TileLayer, Marker } from 'react-leaflet'
import { LeafletMouseEvent } from 'leaflet'

import api from '../../services/api'
import { apiLocations, apiCitiesPerUF } from '../../services/apiIBGE'

import './styles.css'

import logo from '../../assets/logo.svg'

import Dropzone from '../../components/Dropzone'

// References:
// Typescript with React cheats: https://github.com/typescript-cheatsheets/react-typescript-cheatsheet

interface Item {
  id: number;
  title: string;
  image_url: string;
}

interface UF {
  id: number;
  name: string;
  initials: string;
}

interface IBGEUFResponse {
  id: number,
  nome: string;
  sigla: string;
}

interface City {
  id: number;
  name: string;
}

interface IBGECityPerUFResponse {
  id: number,
  nome: string;
}

const CreatePoint = () => {
  const [items, setItems] = useState<Item[]>([])
  const [ufs, setUFs] = useState<UF[]>([])
  const [citiesPerUF, setCitiesPerUF] = useState<City[]>([])

  const [initialPosition, setInitialPosition] = useState<[number, number]>([0,0])
  const [formData, setFormData] = useState({
    name: '',
    email: '',
    whatsapp: '',
  })

  const [selectedUF, setSelectedUF] = useState('0')
  const [selectedCity, setSelectedCity] = useState('0')
  const [selectedItems, setSelectedItems] = useState<number[]>([])
  const [selectedPosition, setSelectedPosition] = useState<[number, number]>([0,0])
  const [selectedFile, setSelectedFile] = useState<File>()

  const history = useHistory()

  // Get Navigator Geolocation
  useEffect(() => {
    navigator.geolocation.getCurrentPosition(position => {
      const { latitude, longitude } = position.coords
      setInitialPosition([latitude, longitude])
    })
  }, [])

  // Get ITEMS from API
  useEffect(() => {
    api.get('items')
    .then(response => setItems(response.data))
  }, [])

  // Get UFs from IBGE API
  useEffect(() => {
    apiLocations.get<IBGEUFResponse[]>('estados?orderBy=nome')
    .then(response => {
      const seriealizedResponse = response.data.map((data: IBGEUFResponse) => {
        const { id, nome, sigla } = data
        return {
          id,
          name: nome,
          initials: sigla,
        }
      })
      setUFs(seriealizedResponse)
    })
  }, [])

  // Get Cities per UF from IBGE API
  useEffect(() => {
    if(selectedUF === '0') return 

    apiCitiesPerUF.get<IBGECityPerUFResponse[]>(`${selectedUF}/municipios?orderBy=nome`)
    .then(response => {
      const seriealizedResponse = response.data.map((data: IBGECityPerUFResponse) => {
        const { id, nome } = data
        return {
          id,
          name: nome,
        }
      })
      setCitiesPerUF(seriealizedResponse)
    })
  }, [selectedUF])

  function handleSelectUF(e: ChangeEvent<HTMLSelectElement>) {
    setSelectedUF(e.target.value)
    setSelectedCity('0') // return city to default value
  }

  function handleSelectCity(e: ChangeEvent<HTMLSelectElement>) {
    setSelectedCity(e.target.value)
  }

  function handleMapClick(e: LeafletMouseEvent) {
    setSelectedPosition([
      e.latlng.lat,
      e.latlng.lng,
    ])
  }

  function handleToggleSelectItem(id: number) {
    const alreadySelected = selectedItems.findIndex(item => item == id)

    const nextSelectedItems = alreadySelected >= 0 ? 
      selectedItems.filter(item => item !== id) :
      [ ...selectedItems, id ]

    setSelectedItems(nextSelectedItems)
  }

  function handleInputChange(e: ChangeEvent<HTMLInputElement>) {
    const { name, value } = e.target
    setFormData({...formData, [name]: value})
  }

  async function handleSubmit(e: FormEvent) {
    e.preventDefault()

    const { name, email, whatsapp } = formData
    const uf = selectedUF
    const city = selectedCity
    const [ latitude, longitude ] = selectedPosition
    const items = selectedItems

    const data = new FormData()

    data.append('name', name) 
    data.append('email', email) 
    data.append('whatsapp', whatsapp)
    data.append('uf', uf)
    data.append('city', city)
    data.append('latitude', String('latitude')) 
    data.append('longitude', String('longitude'))
    data.append('items', items.join(','))
    selectedFile && data.append('image', selectedFile)

    await api.post('points', data)

    // TODO: Create screen to show the success
    alert('Ponto de coleta criado!')

    history.push('/')
  }

  return (
    <div id="page-create-point">
      <header>
        <img src={logo} alt="Ecoleta" />

        <Link to="/">
          <span>
            <FiArrowLeft />
          </span>
          Voltar para home
        </Link>
      </header>

      <form onSubmit={handleSubmit}>
        <h1>Cadastro do <br /> ponto de coleta</h1>

        <Dropzone onFileUploaded={setSelectedFile} />

        {/* TODO: Fazer fieldset com componentização */}
        <fieldset>
          <legend>
            <h2>Dados</h2>
          </legend>

          <div className="field">
            <label htmlFor="name">Nome da entidade</label>
            <input 
              type="text"
              name="name"
              id="name"
              onChange={handleInputChange}
            />
          </div>

          <div className="field-group">
            <div className="field">
              <label htmlFor="email">E-mail</label>
              <input 
                type="text"
                name="email"
                id="email"
                onChange={handleInputChange}
              />
            </div>

            <div className="field">
              <label htmlFor="name">Whatsapp</label>
              <input 
                type="text"
                name="whatsapp"
                id="whatsapp"
                onChange={handleInputChange}
              />
            </div>
          </div>

        </fieldset>

        <fieldset>
          <legend>
            <h2>Endereço</h2>
            <span>Selecione o enderço no mapa</span>
          </legend>

          <Map center={initialPosition} zoom={15} onClick={handleMapClick}>
            <TileLayer
              attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
              url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
            />
            <Marker position={selectedPosition} />
          </Map>

          <div className="field-group">
            <div className="field">
              <label htmlFor="uf">Estado (UF)</label>
              <select 
                name="uf"
                id="uf"
                value={selectedUF}
                onChange={handleSelectUF}
              >
                <option value="0">Selecione uma UF</option>
                {
                  ufs.map(uf => <option key={uf.id} value={uf.initials}>{uf.name}</option>)
                }
              </select>
            </div>

            <div className="field">
              <label htmlFor="city">Cidade</label>
              <select 
                name="city"
                id="city"
                value={selectedCity}
                onChange={handleSelectCity}
              >
                <option value="0">Selecione uma Cidade</option>
                {
                  citiesPerUF.map(city => <option key={city.id} value={city.name}>{city.name}</option>)
                }
              </select>
            </div>
          </div>
        </fieldset>

        <fieldset>
          <legend>
            <h2>Itens de Coleta</h2>
            <span>Selecione um ou mais itens abaixo</span>
          </legend>

          <ul className="items-grid">
            {
              items.map(item => {
                return (
                  <li 
                    key={item.id} 
                    className={selectedItems.includes(item.id) ? 'selected' : ''} 
                    onClick={() => handleToggleSelectItem(item.id)}
                  >
                    {/* TODO: Use local and prod url */}
                    <img src={`http://localhost:3333${item.image_url}`} alt={item.title} />
                    <span>{item.title}</span>
                  </li>
                )
              })
            }
          </ul>

        </fieldset>

        <button type="submit">
          Cadastrar ponto de coleta
        </button>

      </form>
    </div>
  )

}

export default CreatePoint